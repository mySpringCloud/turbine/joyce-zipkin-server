package com.joyce;

import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

import zipkin.server.EnableZipkinServer;
@SpringBootApplication
@EnableZipkinServer
@EnableHystrix
@EnableHystrixDashboard
@EnableEurekaClient
@EnableDiscoveryClient
public class ZipkinServerApplication {
	private static final org.slf4j.Logger LOG = LoggerFactory.getLogger(ZipkinServerApplication.class);

	public static void main(String[] args) { 
		SpringApplication.run(ZipkinServerApplication.class, args); 
	}

}
